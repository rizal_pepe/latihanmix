package com.example.androidlatihanmix_rizalpangestu.data.model

class BukuModel {
    private var title: String? = null
    private var name: String? = null
    private var date: String? = null
    private var desc: String? = null
    private var key: String? = null
    private var image: String? = null

    constructor()
    constructor(name: String, title: String, date: String, desc: String, image: String) {
        this.name = name
        this.title = title
        this.date = date
        this.desc = desc
        this.image = image
    }

    fun getTitle(): String {
        return title!!
    }

    fun getName(): String {
        return name!!
    }

    fun getDate(): String {
        return date!!
    }

    fun getDesc(): String {
        return desc!!
    }


    fun setKey(key: String) {
        this.key = key
    }

    fun getKey(): String {
        return key!!
    }

    fun getImage(): String {
        return image!!
    }

    fun setTitle(title: String) {
        this.title = title
    }

    fun setName(name: String) {
        this.name = name
    }

    fun setDate(date: String) {
        this.date = date
    }

    fun setDesc(desc: String) {
        this.desc = desc
    }
}