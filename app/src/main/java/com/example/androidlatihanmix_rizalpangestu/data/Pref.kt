package com.example.androidlatihanmix_rizalpangestu.data

import android.content.Context
import android.content.SharedPreferences

class Pref(context: Context) {
    private val PREFS_NAME = "TEST"
    val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun setStatusInput(status: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean("STATUS", status)
        editor.apply()
    }

    fun cekStatus(): Boolean {
        return sharedPref.getBoolean("STATUS", false)
    }

    fun setStatusGoogle(statusGoogle: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean("STATUS", statusGoogle)
        editor.apply()
    }

    fun cekStatusGoogle(): Boolean {
        return sharedPref.getBoolean("STATUS", false)
    }

    fun saveCounterId(counter: Int) {
        val edit = sharedPref.edit()
        edit.putInt("IDCOUNTER", counter)
        edit.apply()
    }

    fun getCounterId(): Int {
        return sharedPref.getInt("IDCOUNTER", 1)
    }

    fun saveUid(uid: String) {
        val edit = sharedPref.edit()
        edit.putString("USERID", uid)
        edit.apply()
    }

    fun getUid(): String? {
        return sharedPref.getString("USERID", " ")
    }

}