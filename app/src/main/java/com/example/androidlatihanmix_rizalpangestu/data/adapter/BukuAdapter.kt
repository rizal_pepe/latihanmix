package com.example.androidlatihanmix_rizalpangestu.data.adapter

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.Pref
import com.example.androidlatihanmix_rizalpangestu.data.model.BukuModel
import com.example.androidlatihanmix_rizalpangestu.fragment.HomeFragment
import com.google.firebase.database.DatabaseReference


class BukuAdapter : RecyclerView.Adapter<BukuAdapter.BukuViewHolder> {

    lateinit var mCtx: Context
    lateinit var itemBuku: List<BukuModel>
    lateinit var pref: Pref
    lateinit var listener: FirebaseDataListener
    lateinit var dbRef: DatabaseReference

    constructor()
    constructor(mCtx: Context, list: List<BukuModel>) {
        this.mCtx = mCtx
        this.itemBuku = list
        this.listener = mCtx as HomeFragment
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BukuViewHolder {
        val view: View = LayoutInflater.from(p0.context)
            .inflate(R.layout.show_data, p0, false)
        val bukuViewHolder = BukuViewHolder(view)
        return bukuViewHolder
    }

    override fun getItemCount(): Int {
        return itemBuku.size
    }

    override fun onBindViewHolder(p0: BukuViewHolder, p1: Int) {
        val mBuku: BukuModel = itemBuku.get(p1)
        Glide.with(mCtx).load(mBuku.getImage())
            .centerCrop()
            .error(R.drawable.ic_launcher_background)
            .into(p0.imageRv)
        p0.tv_title.text = mBuku.getTitle()
        p0.tv_name.text = mBuku.getName()
        p0.tv_date.text = mBuku.getDate()
        p0.tv_desc.text = mBuku.getDesc()
        p0.ll.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                val builder = AlertDialog.Builder(mCtx)
                builder.setMessage("Choose Operations")
                builder.setPositiveButton("Update") { dialog, i ->
                    listener.onUpdateData(mBuku, p1)
                }
                builder.setNegativeButton("Delete") { dialog, i ->
                    listener.onDeleteData(mBuku, p1)
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()

                return true
            }

        })
    }

    inner class BukuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ll: LinearLayout
        var imageRv: ImageView
        var tv_title: TextView
        var tv_name: TextView
        var tv_date: TextView
        var tv_desc: TextView

        init {
            ll = itemView.findViewById(R.id.ll)
            imageRv = itemView.findViewById(R.id.imageRv)
            tv_title = itemView.findViewById(R.id.tv_title)
            tv_name = itemView.findViewById(R.id.tv_name)
            tv_date = itemView.findViewById(R.id.tv_date)
            tv_desc = itemView.findViewById(R.id.tv_desc)
        }
    }

    interface FirebaseDataListener {
        fun onDeleteData(bukuModel: BukuModel, position: Int)
        fun onUpdateData(bukuModel: BukuModel, position: Int)
    }
}