package com.example.androidlatihanmix_rizalpangestu.page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.Pref
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {
    lateinit var fAuth: FirebaseAuth
    lateinit var dbRef: DatabaseReference
    lateinit var pref: Pref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        fAuth = FirebaseAuth.getInstance()
        pref = Pref(this)

        btnRegister.setOnClickListener {
            val email = emailRegister.text.toString()
            val password = passwordRegister.text.toString()
            if (email.isNotEmpty() || password.isNotEmpty() ||
                !email.equals("") || !password.equals("")
            ) {
                fAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            addUserToDb(email, password)
                            Toast.makeText(
                                this,
                                "Registrasi Berhasil",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(
                                this,
                                "Registrasi Gagal",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(
                    this,
                    "Email dan Password tidak Boleh Kosong",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        tv_login.setOnClickListener {
            startActivity(Intent(applicationContext, LoginActivity::class.java))
            finish()
        }
    }

    fun addUserToDb(email: String, password: String) {
        dbRef = FirebaseDatabase.getInstance().getReference("user/${fAuth.currentUser?.uid}")
        dbRef.child("/email").setValue(email)
        dbRef.child("/pass").setValue(password)
        pref.setStatusGoogle(false)
        Toast.makeText(
            this,
            "Register Successfull",
            Toast.LENGTH_SHORT
        ).show()

        startActivity(Intent(this, MainActivity::class.java))
    }
}
