package com.example.androidlatihanmix_rizalpangestu.page

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.fragment.HomeFragment
import com.example.androidlatihanmix_rizalpangestu.fragment.UploadFragment
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity() {

    val manager = supportFragmentManager

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                FragmentHome()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_upload -> {
                FragmentUpload()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        FragmentHome()
        profile.setOnClickListener {
            startActivity(Intent(this, User::class.java))
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    fun FragmentHome() {
        val transaction = manager.beginTransaction()
        val fragment = HomeFragment()
        transaction.replace(R.id.holder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun FragmentUpload() {
        val transaction = manager.beginTransaction()
        val fragment = UploadFragment()
        transaction.replace(R.id.holder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
