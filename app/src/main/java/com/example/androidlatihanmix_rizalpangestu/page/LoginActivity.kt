package com.example.androidlatihanmix_rizalpangestu.page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.GetterSetter
import com.example.androidlatihanmix_rizalpangestu.data.Pref
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    //request code
    private val RC_SIGN_IN = 7

    //sign in client
    private lateinit var mGoogleSignIn: GoogleSignInClient

    //firebase auth
    private lateinit var fAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        fAuth = FirebaseAuth.getInstance()

        val pref: Pref = Pref(this)
        val gso = GoogleSignInOptions.Builder(
            GoogleSignInOptions.DEFAULT_SIGN_IN
        ).requestIdToken(
            getString(R.string.default_web_client_id)
        ).requestEmail().build()

        mGoogleSignIn = GoogleSignIn.getClient(this, gso)

        google_button.setOnClickListener {
            pref.setStatusInput(true)
            pref.setStatusGoogle(true)
            val signInIntent = mGoogleSignIn.signInIntent
//            GetterSetter.setusername("${fAuth.currentUser!!.displayName}")
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        tv_register.setOnClickListener {
            Toast.makeText(this, "Register Page", Toast.LENGTH_SHORT)
                .show()
            startActivity(Intent(applicationContext, Register::class.java))
        }
        btnLogin.setOnClickListener {
            val username = usernameLogin.text.toString()
            val password = passwordLogin.text.toString()
            when {
                username.isEmpty() -> Toast.makeText(this, "Username Kosong", Toast.LENGTH_SHORT)
                    .show()
                password.length < 6 -> Toast.makeText(this, "Password Terlalu Pendek ( Min 6 )", Toast.LENGTH_SHORT)
                    .show()
                else -> {
                    pref.setStatusInput(true)
                    pref.setStatusGoogle(false)
                    GetterSetter.setusername(usernameLogin.text.toString())
                    fAuth.signInWithEmailAndPassword(username, password)
                        .addOnSuccessListener {
                            startActivity(
                                Intent(
                                    this,
                                    Home::class.java
                                )
                            )
                        }
                        .addOnFailureListener {
                            Toast.makeText(
                                this,
                                "LOGIN GAGAL",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
            }
        }
    }


    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        Log.d(
            "Firebase_Auth_Login",
            "firebaseAuth : ${account.id}"
        )

        val credential = GoogleAuthProvider.getCredential(
            account.idToken, null
        )

        fAuth.signInWithCredential(credential).addOnCompleteListener(this) {
            if (it.isSuccessful) {
                val user = fAuth.currentUser
                updateUI(user)
            } else {
                Toast.makeText(
                    this,
                    "LOGIN GAGAL",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Toast.makeText(
                this,
                "Login Berhasil Welcome " +
                        "${user.displayName}",
                Toast.LENGTH_SHORT
            ).show()
            startActivity(
                Intent(this, MainActivity::class.java)
            )
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn
                .getSignedInAccountFromIntent(data)
            try {
                val account = task
                    .getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Log.e(
                    "AUTH_LOGIN",
                    "LOGIN_GAGAL", e
                )
            }
        }
    }

}

