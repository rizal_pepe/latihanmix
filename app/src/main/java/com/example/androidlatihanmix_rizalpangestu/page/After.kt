package com.example.androidlatihanmix_rizalpangestu.page

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.GetterSetter
import kotlinx.android.synthetic.main.activity_after.*

class After : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_after)
        var kota: String? = ""
        val b = intent.extras
        if (b != null) {
            kota = b.getString("kota")
        }
        if (kota == "jakarta") run {
            name.text = GetterSetter.getusername()
            fotolur.setImageResource(R.drawable.jkt)
            lokasi.setText(R.string.jakarta)
            desc.setText(R.string.descjakarta)
        } else if (kota == "bandung") run {
            name.text = GetterSetter.getusername()
            fotolur.setImageResource(R.drawable.bdg)
            lokasi.setText(R.string.bandung)
            desc.setText(R.string.descbandung)
        } else if (kota == "surabaya") run {
            name.text = GetterSetter.getusername()
            fotolur.setImageResource(R.drawable.sby)
            lokasi.setText(R.string.surabaya)
            desc.setText(R.string.descsurabaya)
        } else if (kota == "malang") run {
            name.text = GetterSetter.getusername()
            fotolur.setImageResource(R.drawable.mlg)
            lokasi.setText(R.string.malang)
            desc.setText(R.string.descmalang)
        } else if (kota == "padang") {
            name.text = GetterSetter.getusername()
            fotolur.setImageResource(R.drawable.pdg)
            lokasi.setText(R.string.padang)
            desc.setText(R.string.descpadang)
        }
    }
}
