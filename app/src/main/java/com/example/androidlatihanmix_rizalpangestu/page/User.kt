package com.example.androidlatihanmix_rizalpangestu.page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.GetterSetter
import com.example.androidlatihanmix_rizalpangestu.data.Pref
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_user.*

class User : AppCompatActivity() {

    lateinit var fAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        val pref: Pref = Pref(this)
        fAuth = FirebaseAuth.getInstance()
        if (!(pref.cekStatusGoogle())) {
            usernameProfile.text = GetterSetter.getusername()
        } else {
            usernameProfile.text = "${fAuth.currentUser!!.displayName}"
        }
        back.setOnClickListener {
            onBackPressed()
        }
        logout.setOnClickListener {
            pref.setStatusInput(false)
            fAuth.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
