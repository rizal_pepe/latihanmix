package com.example.androidlatihanmix_rizalpangestu.Page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.androidlatihanmix_rizalpangestu.Data.GetterSetter
import com.example.androidlatihanmix_rizalpangestu.Data.Pref
import com.example.androidlatihanmix_rizalpangestu.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val pref: Pref = Pref(this)
        tv_register.setOnClickListener {
            Toast.makeText(this, "Register Page", Toast.LENGTH_SHORT)
                .show()
            startActivity(Intent(applicationContext, Register::class.java))
        }
        google_button.setOnClickListener {
            startActivity(Intent(applicationContext, Home::class.java))
        }
        btnLogin.setOnClickListener {
            val username = usernameLogin.text.toString()
            val password = passwordLogin.text.toString()
            when {
                username.isEmpty() -> Toast.makeText(this, "Username Kosong", Toast.LENGTH_SHORT)
                    .show()
                password.length < 6 -> Toast.makeText(this, "Password Terlalu Pendek ( Min 6 )", Toast.LENGTH_SHORT)
                    .show()
                else -> {
                    pref.setStatusInput(true)
                    GetterSetter.setusername(usernameLogin.text.toString())
                    Toast.makeText(this, "Login Sukses", Toast.LENGTH_SHORT)
                        .show()
                    startActivity(Intent(this, Home::class.java))
                    finish()
                }
            }
        }
    }
}
