package com.example.androidlatihanmix_rizalpangestu.Page

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.androidlatihanmix_rizalpangestu.R
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        tv_login.setOnClickListener {
            startActivity(Intent(applicationContext, LoginActivity::class.java))
            finish()
        }

        google_button.setOnClickListener {
            startActivity(Intent(applicationContext, Home::class.java))
        }
    }
}
