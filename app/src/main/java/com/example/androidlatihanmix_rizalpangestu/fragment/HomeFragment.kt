package com.example.androidlatihanmix_rizalpangestu.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.androidlatihanmix_rizalpangestu.R
import com.example.androidlatihanmix_rizalpangestu.data.Pref
import com.example.androidlatihanmix_rizalpangestu.data.adapter.BukuAdapter
import com.example.androidlatihanmix_rizalpangestu.data.model.BukuModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.util.*


class HomeFragment : Fragment(), BukuAdapter.FirebaseDataListener {
    override fun onDeleteData(bukuModel: BukuModel, position: Int) {
        dbRef = FirebaseDatabase.getInstance()
            .getReference("buku/${pref.getUid()}")
        dbRef.child(bukuModel.getKey()).removeValue()
            .addOnSuccessListener {
                Toast.makeText(
                    context,
                    "Delete Success",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    override fun onUpdateData(bukuModel: BukuModel, position: Int) {
//        val dataxxx = bukuModel.getKey()
//        val intent = Intent(
//            context,
////            UpdateData::class.java
//        )
//        intent.putExtra("kode", dataxxx)
//        startActivity(intent)
    }

    private lateinit var fAuth: FirebaseAuth
    private var bukuAdapter: BukuAdapter? = null
    private var recyclerView: RecyclerView? = null
    private var list: MutableList<BukuModel> = ArrayList<BukuModel>()
    lateinit var dbRef: DatabaseReference
    lateinit var pref: Pref
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.setHasFixedSize(true)

        pref = Pref(context!!)
        fAuth = FirebaseAuth.getInstance()

        dbRef = FirebaseDatabase.getInstance()
            .getReference("buku/${pref.getUid()}")
        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                list = ArrayList()
                for (dataSnapshot in p0.children) {
                    val addDataAll = dataSnapshot.getValue(
                        BukuModel::class.java
                    )
                    addDataAll!!.setKey(dataSnapshot.key!!)
                    list.add(addDataAll)
                    bukuAdapter = BukuAdapter(context!!, list)
                    recyclerView!!.adapter = bukuAdapter
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                Log.e(
                    "TAG_ERROR", p0.message
                )
            }

        })
    }
}
